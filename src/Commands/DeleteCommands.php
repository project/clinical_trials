<?php

namespace Drupal\clinical_trials\Commands;

use Drupal\clinical_trials\ClinicalDeleteService;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Class DeleteCommands.
 *
 * Custom drush command to delete the unwanted clinical trials data.
 *
 * @package Drupal\clinical_trials
 */
class DeleteCommands extends DrushCommands {

  /**
   * Entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  private $entityManager;

  /**
   * The service for clinical trials.
   *
   * @var \Drupal\clinial_trials\ClinicalDeleteService
   */
  protected $clinicalService;

  /**
   * Constructs a new ImportCommands object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entityManager
   *   The Entity Type Manager service.
   * @param \Drupal\clinial_trials\ClinicalDeleteService $clinicalService
   *   Clinical Trial Delete Service.
   */
  public function __construct(EntityTypeManagerInterface $entityManager, ClinicalDeleteService $clinicalService) {
    $this->entityManager = $entityManager;
    $this->clinicalService = $clinicalService;
  }

  /**
   * Drush command which Imports Ctapi data.
   *
   * @command clinical-trial:deleteStudies
   * @aliases ct-delete-studies
   */
  public function doDelete() {
    $this->clinicalService->deleteCtData();
  }

}
