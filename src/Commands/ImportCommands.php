<?php

namespace Drupal\clinical_trials\Commands;

use Drupal\clinical_trials\ClinicalImportService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drush\Commands\DrushCommands;

/**
 * Class ImportCommands.
 *
 * Custom drush command to import clinical trials data.
 *
 * @package Drupal\clinical_trials
 */
class ImportCommands extends DrushCommands {
  /**
   * The configuration object factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

  /**
   * The service for clinical trials.
   *
   * @var \Drupal\clinial_trials\ClinicalImportService
   */
  protected $clinicalService;

  /**
   * Constructs a new ImportCommands object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configfactory
   *   The configuration object factory.
   * @param \Drupal\clinial_trials\ClinicalImportService $clinicalservice
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $configfactory, ClinicalImportService $clinicalservice) {
    $this->configFactory = $configfactory;
    $this->clinicalService = $clinicalservice;
    $this->clinicalConfig = $this->configFactory->getEditable('clinical_trials.settings');
  }

  /**
   * Drush command which Imports Ctapi data.
   *
   * @command clinical-trial:importStudies
   * @aliases ct-import-studies
   */
  public function importCtStudies() {
    $needed_clinical_config = [
      'base_url',
      'studies_api_url',
      'lead',
      'overallstatus',
      'fields',
      'page_size',
      'markup_format',
    ];
    $do_import = TRUE;
    $empty_config_message = '';
    // Before triggering the import first check the needed config is available.
    // Start import process only when all the needed basic config is available.
    foreach ($needed_clinical_config as $clinical_config_name) {
      $clinical_config_value = $this->clinicalConfig->get($clinical_config_name);
      if (empty($clinical_config_value)) {
        $do_import = FALSE;
        $empty_config_message = "Following config is empty please configure them to start the import: " . $clinical_config_name;
        break;
      }
    }
    if ($do_import == TRUE) {
      $ct_count = $this->clinicalService->getCtCount();
      if (!empty($ct_count) && $ct_count > 0) {
        $this->output()->writeln('Total CT Count: ' . $ct_count);
        $this->clinicalService->getCtData($ct_count);
      }
      else {
        $this->output()->writeln('Something went wrong. Please check the logs for more details.');
      }
    }
    else {
      $this->output()->writeln($empty_config_message);
    }
  }

}
