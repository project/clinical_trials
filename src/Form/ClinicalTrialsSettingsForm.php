<?php

namespace Drupal\clinical_trials\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class ClinicalTrialsSettingsForm extends ConfigFormBase {


  /**
   * Config settings key.
   */
  const SETTINGS = 'clinical_trials.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'clinical_trials_configuration';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(static::SETTINGS);

    $form['base_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Studies API URL'),
      '#default_value' => $config->get('base_url') ?? "https://clinicaltrials.gov/api/v2",
      '#description' => $this->t('Api Base URL'),
    ];
    $form['studies'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Studies'),
    ];
    $form['studies']['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t('
      Returns data of studies matching query and filter parameters. The studies are returned page by page. If response contains <b>nextPageToken</b>, use its value in <b>pageToken</b> to get next page. The last page will not contain <b>nextPageToken</b>. A page may have empty <b>studies</b> array. Request for each subsequent page must have the same parameters as for the first page, except <b>countTotal</b>, <b>pageSize</b>, and <b>pageToken</b> parameters.<br></br>
      If neither queries nor filters are set, all studies will be returned. If any query parameter contains only NCT IDs (comma- and/or space-separated), filters are ignored.<br></br>
      <b>query.*</b> parameters are in Essie expression syntax. Those parameters affect ranking of studies, if sorted by relevance. See sort parameter for details.<br></br>
      <b>filter.*</b> and <b>postFilter.*</b> parameters have same effect as there is no aggregation calculation. Both are available just to simplify applying parameters from search request. Both do not affect ranking of studies.<br></br>
      Note: When trying JSON format in your browser, do not set too large pageSize parameter, if fields is unlimited. That may return too much data for the browser to parse and render.
     <br></br><b>REQUEST QUERY-STRING PARAMETERS</b>
      '),
    ];

    $form['studies']['studies_api_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Studies API URL'),
      '#default_value' => $config->get('studies_api_url') ?? "/studies",
      '#description' => $this->t('Api End point'),
    ];
    $form['studies']['lead'] = [
      '#type' => 'textarea',
     // '#required' => TRUE,
      '#title' => $this->t('query.lead'),
      '#default_value' => $config->get('lead') ?? "",
      '#description' => $this->t('Searches in "LeadSponsorName" field. See Study Data Structure for more details. The query is in Essie expression syntax.'),
    ];

    $form['studies']['overallstatus'] = [
      '#type' => 'textfield',
      // '#required' => TRUE,
      '#title' => $this->t('filter.overallStatus'),
      '#default_value' => $config->get('overallstatus') ?? "",
      '#description' => $this->t('Allowed: ACTIVE_NOT_RECRUITING ┃ COMPLETED ┃ ENROLLING_BY_INVITATION ┃ NOT_YET_RECRUITING ┃ RECRUITING ┃ SUSPENDED ┃ TERMINATED ┃ WITHDRAWN ┃ AVAILABLE ┃ NO_LONGER_AVAILABLE ┃ TEMPORARILY_NOT_AVAILABLE ┃ APPROVED_FOR_MARKETING ┃ WITHHELD ┃ UNKNOWN'),
    ];
    $form['studies']['fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('fields'),
      '#default_value' => $config->get('fields') ?? "",
      '#description' => $this->t('If specified, must be non-empty comma- or pipe-separated list of fields to return. If unspecified, all fields will be returned. Order of the fields does not matter.<br><br>
      For json format, every list item is either area name, piece name, field name, or special name. If a piece or a field is a branch node, all descendant fields will be included. All area names are available on Search Areas, the piece and field names — on Data Structure and also can be retrieved at /studies/metadata endpoint. There is a special name, @query, which expands to all fields queried by search.<br><br>
      <b>Examples: [ NCTId, BriefTitle, OverallStatus, HasResults ] ┃ [ ProtocolSection ]</b>
      '),
    ];

    $form['studies']['page_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('pageSize'),
      '#default_value' => $config->get('page_size') ?? "",
      '#description' => $this->t('Page size is maximum number of studies to return in response. It does not have to be the same for every page. If not specified or set to 0, the default value will be used. It will be coerced down to 1,000, if greater than that.<br><br>
      <b>Examples:</b> 2 ┃ 100'),
    ];
    $form['studies']['markup_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('MarkupFormat'),
      '#default_value' => $config->get('markup_format') ?? "",
      '#description' => $this->t('Format of markup type fields: <br></br> <b>markdown</b> -  markdown format<br>
      <b>legacy</b> - compatible with classic PRS
      '),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('lead', $form_state->getValue('lead'))
      ->set('overallstatus', $form_state->getValue('overallstatus'))
      ->set('base_url', $form_state->getValue('base_url'))
      ->set('studies_api_url', $form_state->getValue('studies_api_url'))
      ->set('page_size', $form_state->getValue('page_size'))
      ->set('fields', $form_state->getValue('fields'))
      ->set('markup_format', $form_state->getValue('markup_format'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
