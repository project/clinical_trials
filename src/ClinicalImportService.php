<?php

namespace Drupal\clinical_trials;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * General Methods.
 */
class ClinicalImportService {
  use MessengerTrait;
  use StringTranslationTrait;
  use DependencySerializationTrait;
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The logger channel Factory.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * The database object.
   *
   * @var object
   */
  protected $database;

  /**
   * Entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  private $entityManager;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private StateInterface $state;

  /**
   * The clinical config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $clinicalConfig;

  /**
   * The API URL.
   *
   * @var string
   */
  private $apiUrl;

  /**
   * The API method.
   *
   * @var string
   */
  protected $apiMethod;

  /**
   * The base API parameters.
   *
   * @var array
   */
  protected $baseApiParams;

  /**
   * Creates a verbose messenger.
   */
  public function __construct(Connection $database, Client $httpClient, LoggerChannelFactoryInterface $loggerChannelFactory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityManager, StateInterface $state) {
    $this->database = $database;
    $this->httpClient = $httpClient;
    $this->logger = $loggerChannelFactory;
    $this->configFactory = $config_factory;
    $this->entityManager = $entityManager;
    $this->state = $state;
    $this->clinicalConfig = $this->configFactory->getEditable('clinical_trials.settings');
    $base_url = $this->clinicalConfig->get('base_url') ?? 'https://clinicaltrials.gov/api/v2/';
    $studies_url = $this->clinicalConfig->get('studies_api_url') ?? "/studies";
    $this->apiUrl = $base_url . $studies_url;
    $this->apiMethod = 'get';
    $this->baseApiParams = [
      'query.lead' => $this->clinicalConfig->get('lead') ?? "",
      'filter.overallStatus' => $this->clinicalConfig->get('overallstatus') ?? "",
      'countTotal' => 'true',
      'markupFormat' => $this->clinicalConfig->get('markup_format') ?? "legacy",
    ];
  }

  /**
   * Helper function to get the total count.
   */
  public function getCtCount() {
    $count_param = $this->baseApiParams;
    $count_param['pageSize'] = 1;
    $url_obj = Url::fromUri($this->apiUrl);
    $url_obj->setOptions(['query' => $count_param]);
    $url = $url_obj->toString();
    $getCount = $this->triggerCtApiCalls($this->apiMethod, $url, $count_param);
    $count = FALSE;
    if (!empty($getCount)) {
      $count = $getCount['totalCount'];
    }
    return $count;
  }

  /**
   * Fetching CT api data.
   *
   * @param int $count
   *   Total Count of the Ct api.
   */
  public function getCtData($count) {

    $param = $this->baseApiParams;
    $batch_item_count = $this->clinicalConfig->get('page_size') ?? 10;
    $param['pageSize'] = $batch_item_count;
    $fields_to_fetch = $this->clinicalConfig->get("fields");
    if (!empty($fields_to_fetch)) {
      $param['fields'] = $this->clinicalConfig->get("fields");
    }
    $loop_count = ceil($count / $batch_item_count);
    $operations = [
      'title' => $this->t('Fetching CT API.'),
      'finished' => 'Drupal\clinical_trials\ClinicalImportService::processCtBatchFinished',
    ];
    for ($i = 1; $i <= $loop_count; $i++) {
      $operations['operations'][] = [
        [$this, 'processCtBatchInit'],
        [
          $param,
        ],
      ];
    }
    batch_set($operations);
    if (function_exists('drush_backend_batch_process')) {
      drush_backend_batch_process();
    }
  }

  /**
   * Trigger API Calls.
   */
  public function triggerCtApiCalls($method, $url, $arg) {
    $ct_api_response = "";
    try {
      $ct_response = $this->httpClient->request($method, $url, $arg);
      $ct_response_code = $ct_response->getStatusCode();
      // Retry on 403 error.
      if ($ct_response_code == 403) {
        $ct_response = $this->httpClient->request($method, $url, $arg);
        $ct_response_code = $ct_response->getStatusCode();
      }
      if ($ct_response_code == 200) {
        $ct_content = $ct_response->getBody()->getContents();
        $ct_api_response = Json::decode($ct_content);
      }
    }
    catch (GuzzleException $exception) {
      $ct_logger = $this->logger->get('Clinical Trials');
      $ct_logger->error($exception->getMessage());
    }

    return $ct_api_response;
  }

  /**
   * Initializing Batch.
   *
   * @param array $param
   *   Parameters for api call.
   * @param mixed $context
   *   Context pass between batch.
   */
  public function processCtBatchInit($param, &$context) {

    if (!empty($context['results']['next_token'])) {
      $param['pageToken'] = $context['results']['next_token'];
    }
    else {
      $context['results']['next_token'] = '';
    }
    $url_obj = Url::fromUri($this->apiUrl);
    $url_obj->setOptions(['query' => $param]);
    $base_url = $url_obj->toString();
    $getData = $this->triggerCtApiCalls($this->apiMethod, $base_url, $param);

    if (!empty($getData['nextPageToken'])) {
      $context['results']['next_token'] = $getData['nextPageToken'];
    }
    $next_token = $getData['nextPageToken'] ?? "";
    if (!isset($context['results']['created_count'])) {
      $context['results']['created_count'] = 0;
    }

    if (!isset($context['results']['updated_count'])) {
      $context['results']['updated_count'] = 0;
    }
    if (!isset($context['results']['created_nct_id'])) {
      $context['results']['created_nct_id'] = [];
    }
    foreach ($getData['studies'] as $study) {
      $this->createCtNode($study, $context);
    }
    $context['message'] = $this->t('Next Token to process @token', ['@token' => $next_token]);
  }

  /**
   * Helper function to create CT nodes.
   *
   * @param array $study
   *   Study array.
   * @param mixed $context
   *   Context pass between batch.
   */
  private function createCtNode($study, &$context) {

    $node_storage = $this->entityManager->getStorage('node');
    $protocol = $study['protocolSection'];
    $logger = $this->logger->get('Clinical Trials');
    if (!empty($protocol)) {
      $message = $this->t('Clinical trial Data started for @token', ['@token' => $protocol['identificationModule']['nctId']]);
      $logger->notice($message);
      $nct_id = $protocol['identificationModule']['nctId'];
      $node = $node_storage->loadByProperties([
        'type' => 'clinicaltrials',
        'title' => $nct_id,
      ]);

      $context['results']['created_nct_id'][] = $nct_id;

      $ct_node_array = $this->setCtNodeMapping($protocol);
      if (empty($node)) {
        try {
          $node = $this->entityManager->getStorage('node')->create($ct_node_array);
          $node->setPublished(TRUE);
          $node->save();
          $message = $this->t('Clinical trial Data Completed for @nct_id', ['@nct_id' => $protocol['identificationModule']['nctId']]);
          $logger->notice($message);
          $context['results']['created_count']++;
        }
        catch (\Exception $e) {
          $logger->error('Error during node entity import : @error_message', ['@error_message' => $e->getMessage()]);
        }
      }
      else {
        $existing_node = reset($node);
        $existing_node_array = $this->setCtNodeMapping($protocol, $existing_node->id());
        foreach ($existing_node_array as $field_name => $field_value) {
          $existing_node->set($field_name, $field_value);
        }
        $existing_node->setPublished(TRUE);
        $existing_node->save();
        $message = $this->t('Clinical trial Data Completed for @nct_id', ['@nct_id' => $protocol['identificationModule']['nctId']]);
        $logger->notice($message);
        $context['results']['updated_count']++;
      }
    }
  }

  /**
   * Helper function to set CT node fields.
   *
   * @param array $protocol_array
   *   Study array.
   * @param int $nid
   *   Nid of the existing node.
   */
  private function setCtNodeMapping($protocol_array, $nid = NULL) {

    $title = $protocol_array['identificationModule']['nctId'] ?? "";
    if (!empty($title) && (strlen($title) > 255)) {
      $title = substr($title, 0, 250) . '...';
    }
    $returned_data = [];
    if (!empty($protocol_array)) {
      $data = serialize($protocol_array);
      $returned_data = [
        'type' => 'clinicaltrials',
        'title' => $title,
        'field_data' => $data,
        'uid' => 1,
      ];
    }
    return $returned_data;
  }

  /**
   * Batch callback finish function.
   *
   * @param bool $success
   *   Success or failure flag.
   * @param array $results
   *   Results array.
   */
  public static function processCtBatchFinished(bool $success, array $results): void {

    // Setting the Current batch nct ids and already created nct ids.
    // In a variable using state API.
    // So we can easily delete the unwanted nodes in future.
    $current_ct_cid = 'clinical_trials_current_nct_id';
    $previous_ct_cid = 'clinical_trials_previous_nct_id';

    $current_ct_state = \Drupal::state()->get($current_ct_cid);
    $previous_ct_state = \Drupal::state()->get($previous_ct_cid);
    if ($success) {
      if (!empty($results['created_nct_id'])) {
        $created_nct_ids = implode(",", $results['created_nct_id']);
        if (empty($current_ct_state) && empty($previous_ct_state)) {
          \Drupal::state()->set($current_ct_cid, $created_nct_ids);
        }
        elseif (!empty($current_ct_state) && empty($previous_ct_state)) {
          \Drupal::state()->set($previous_ct_cid, $current_ct_state);
          \Drupal::state()->set($current_ct_cid, $created_nct_ids);
        }
      }

      $message = t('New Contents Created: @created_count, Updated Existing Contents: @updated_count.', [
        '@created_count' => $results['created_count'],
        '@updated_count' => $results['updated_count'],
      ]);
    }
    else {
      $message = t('Finished with an error.');
    }

    \Drupal::messenger()->addMessage($message);
  }

}
