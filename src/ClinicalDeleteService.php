<?php

namespace Drupal\clinical_trials;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * General Methods.
 */
class ClinicalDeleteService {
  use StringTranslationTrait;
  use DependencySerializationTrait;
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger channel Factory.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;


  /**
   * Entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  private $entityManager;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private StateInterface $state;

  /**
   * Creates a verbose messenger.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityManager, StateInterface $state) {
    $this->logger = $loggerChannelFactory;
    $this->configFactory = $config_factory;
    $this->entityManager = $entityManager;
    $this->state = $state;
  }

  /**
   * Deleting ct data.
   */
  public function deleteCtData() {

    $current_ct_cid = 'clinical_trials_current_nct_id';
    $previous_ct_cid = 'clinical_trials_previous_nct_id';
    $current_ct_state = $this->state->get($current_ct_cid);
    $previous_ct_state = $this->state->get($previous_ct_cid);

    if (!empty($current_ct_state) && !empty($previous_ct_state)) {
      $current_ct_ids = explode(",", $current_ct_state);
      $previous_ct_ids = explode(",", $previous_ct_state);
      if (!empty($current_ct_ids) && !empty($previous_ct_ids)) {
        $diff_ct = array_diff($previous_ct_ids, $current_ct_ids);
      }

      $operations = [
        'title' => $this->t('Deleting CT Nodes.'),
        'finished' => 'Drupal\clinical_trials\ClinicalDeleteService::processCtDeleteFinished',
      ];

      if (!empty($diff_ct)) {
        foreach ($diff_ct as $nct_id) {
          $operations['operations'][] = [
              [$this, 'processCtDeleteBatchInit'],
              [
                $nct_id,
              ],
          ];
        }
        batch_set($operations);
        if (function_exists('drush_backend_batch_process')) {
          drush_backend_batch_process();
        }
      }
      else {
        $message = $this->t('No Content to delete');
        $state_delete_array = ['clinical_trials_previous_nct_id'];
        $this->deleteCtStates($state_delete_array);
        \Drupal::messenger()->addMessage($message);
      }

    }
  }

  /**
   * Helper function to Delete CT nodes.
   *
   * @param int $nct_id
   *   Nct id to delete.
   * @param mixed $context
   *   Context pass between batch.
   */
  public function processCtDeleteBatchInit($nct_id, &$context) {
    if (!isset($context['results']['deleted_count'])) {
      $context['results']['deleted_count'] = 0;
    }
    $node_storage = $this->entityManager->getStorage("node");
    $loaded_node_array = $node_storage->loadByProperties([
      'type' => 'clinicaltrials',
      'title' => $nct_id,
    ]);
    $existing_node = reset($loaded_node_array);
    if (!empty($existing_node)) {
      $existing_node->delete();
      $context['message'] = $this->t('Successfully Deleted @nct_id', ['@nct_id' => $nct_id]);
      $context['results']['deleted_count']++;
    }
  }

  /**
   * Batch callback finish function.
   *
   * @param bool $success
   *   Success or failure flag.
   * @param array $results
   *   Results array.
   */
  public static function processCtDeleteFinished(bool $success, array $results): void {

    $state_cid = ['clinical_trials_previous_nct_id'];
    if ($success) {
      ClinicalDeleteService::deleteCtStates($state_cid);
      $message = 'Deleted CT Node Count:' . $results['deleted_count'];
    }
    else {
      $message = 'Finished with an error.';
    }
    \Drupal::messenger()->addMessage($message);
  }

  /**
   * Helper function to delete the Ct states.
   *
   * @param array $cid_array
   *   Cid array to delete.
   */
  public static function deleteCtStates($cid_array) {
    $deleted_cid = FALSE;
    if (!empty($cid_array)) {
      foreach ($cid_array as $cid) {
        \Drupal::state()->delete($cid);
        $deleted_cid = TRUE;
      }
    }
    return $deleted_cid;
  }

}
