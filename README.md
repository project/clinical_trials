# ClinicalTrials.gov Integration


This module integrates with the open-source data from [Clinical Trials.gov](https://clinicaltrials.gov/data-api/api). Stores it as a content in the Drupal System.




Setup
-----



- Enable this module.

- Configure the module at /admin/config/clinical-trials/settings-form.
 
- Once configuration done we can run the drush command to import the clinical trial data
```shell
drush ct-import-studies
```